function Debug() {
    var open = true;

    this.debug = function() {
        return open;
    };

    this.Log = function(tag, info) {
        if (open) {
            if (typeof(info) == 'object') {
                console.log(tag, info);
            } else {
                console.log(tag + info);
            }
        }
    }
};

var Debug = new Debug();
